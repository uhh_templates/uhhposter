Latex Poster Template for UHH
=============================

A latex template to efficently design pretty posters. Posters are composited of
blocks with headings, that can be positioned easily on the page, using absolute
or relative positioning.

This version was adapted from the [original baposter](http://www.brian-amberg.de/uni/poster/)
to fit the style guidelines of Universität Hamburg.  Note that the UHH logo is *not*
freely licensed; you may only use it if appropriate (e.g. you are a
member of UHH).

It looks like this:

![UHH example poster](koehn-coling-2018.png)

This modified version requires luatex for proper font support.  You
need to have the TheSansUHH fonts available *in the directory of your tex file*.

Download [The university fonts](http://online-dienste.verwaltung.uni-hamburg.de/projekte/fonts/lizenzvereinbarung.html) and save them as:
  - `TheSansUHH-Regular.ttf`
  - `TheSansUHH-Regular-Italic.ttf`
  - `TheSansUHH-Bold-Italic.ttf`
  - `TheSansUHH-Bold.ttf`
  - `TheSansUHH-BoldCaps.ttf`
  - `TheSansUHH-SemiLightCaps.ttf`


Scaling is performed in UHH-logo-heights: set the key uhhlogoheight to
the percentage of the height of the poster, the title box and
"erweiterter Absender" (box bottom right) will scale accordingly.

The key titleboxheight is measured in uhhlogoheight. Default 3: 3
times the height of the UHH logo.

Use \UHHTable to switch to the theSansUHH font with monospaced digits
for proper alignment of numbers in tables.

Some uhh colors are defined:

blueuhh
reduhh
greyuhh
bluetextuhh
greentextuhh
lightgreyuhh (used for the grey column)

Have a look at the koehn-coling-2018.tex example file if you don't
want to start from scratch. See See baposter_guide for a guide to baposter.

Contributing
------------

If you make changes to the template that might be useful to other people as well, please share your work:

https://gitlab.com/uhh_templates/uhhposter

    Licence: GPLv3
    (c) 2007-2011 Brian Amberg and Reinhold Kainhofer
    (c) 2017-2018 Arne Köhn <arne@chark.eu>
